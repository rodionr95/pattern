let textElement = document.getElementById('updateText');
let singletonInstance = null;

class SingletonExample {
  constructor() {
    // Check if the instance exists or is null
    if (!singletonInstance) {
      // If null, set singletonInstance to this Class 
      singletonInstance = this;
      textElement.textContent = "Singleton Class Created!";
    } else {
      textElement.innerHTML += "<br>Whoopsie, you're only allowed one instance of this Class!";
    }

    // Returns the initiated Class
    return singletonInstance;
  }
  
  save(name) {
    localStorage.setItem('myCat',  JSON.stringify({name}));
  }
  read() {
    var cat = localStorage.getItem('myCat');
    return cat;
  }
  delete() {
    localStorage.removeItem('myCat');
  }
  
  }

// Create a new instance of singleton Class
let singletonExample = new SingletonExample();
let singletonExample2 = new SingletonExample();

singletonExample.save('barbosik');
singletonExample.save('barsik');
singletonExample.save('snejok');
